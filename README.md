# Dependency Walker #

`Dependency Walker` is a small application to create a vision representation of includes. The current version supports Xcode Projects and includes and imports. As a result a .dot file will be created.

## Usage

`dependencyWalker <input-file> <output-file>`

## Licenses
'Dependency Walker' is available under the MIT license. See the LICENSE file for more info.