//
//  ObjCDependencyExtractor.m
//  DependencyWalker
//
//  Created by Jan Olbrich on 04.07.15.
//  Copyright (c) 2015 Jan Olbrich. All rights reserved.
//

#import "ObjCDependencyExtractor.h"
#import "DependencyStore.h"
#import "FileDependency.h"

@interface ObjCDependencyExtractor ()

@property (nonatomic, strong) DependencyStore *store;

@end

@implementation ObjCDependencyExtractor

- (DependencyStore *)store {
    if (!_store) {
        _store = [DependencyStore new];
    }
    
    return _store;
}

- (NSArray *)dependenciesForFileList:(NSArray *)fileList {
    
    for (NSString *filePath in fileList) {
        [self dependencyForFile:filePath];
    }
    
    return [self.store dependencies];
}

- (void)dependencyForFile:(NSString *)file {
    NSError *error = nil;
    NSString *pattern = @"(#import|#include) \"(.+)\\.h\"";
    NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
    
    NSString *fileContents = [NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:NULL];
    FileDependency *dependencies = [self.store dependencyWithFile:file];
    
    for (NSString *line in [fileContents componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]]) {
        NSTextCheckingResult *textCheckingResult = [expression firstMatchInString:line
                                                                          options:0
                                                                            range:NSMakeRange(0, line.length)];
        if (!textCheckingResult) {
            continue;
        }
        
        NSString* dependency = [line substringWithRange:[textCheckingResult rangeAtIndex:2]];
        [dependencies addFileDependency:dependency];
    }
    
    NSLog(@"File: %@ LOC: %lu", dependencies.name, (unsigned long)dependencies.loc);
}

@end
