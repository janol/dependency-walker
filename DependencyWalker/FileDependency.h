//
//  FileDependency.h
//  DependencyWalker
//
//  Created by Jan Olbrich on 04.07.15.
//  Copyright (c) 2015 Jan Olbrich. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileDependency : NSObject

@property (nonatomic, strong, readonly) NSString *name;
@property (nonatomic) NSUInteger loc;

- (instancetype)initWithName:(NSString *)fileName;
- (void)addFileDependency:(NSString *)file;
- (NSArray *)fileDependencies;

@end
