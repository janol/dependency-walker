//
//  main.m
//  DependencyWalker
//
//  Created by Jan Olbrich on 04.07.15.
//  Copyright (c) 2015 Jan Olbrich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XCodeParser.h"
#import "ObjCDependencyExtractor.h"
#import "GraphVizDependencyGrapher.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        XCodeParser *parser = [XCodeParser new];
        NSArray *fileList = [parser fileListFromProjectPath:[[NSProcessInfo processInfo] arguments][1]];
        
        ObjCDependencyExtractor *extractor = [ObjCDependencyExtractor new];
        NSArray *fileDependencies = [extractor dependenciesForFileList:fileList];
        
        GraphVizDependencyGrapher *grapher = [GraphVizDependencyGrapher new];
        [grapher graphDepencies:fileDependencies toFile:[[NSProcessInfo processInfo] arguments][2]];
    }
    return 0;
}
