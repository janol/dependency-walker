//
//  DependencyStore.m
//  DependencyWalker
//
//  Created by Jan Olbrich on 04.07.15.
//  Copyright (c) 2015 Jan Olbrich. All rights reserved.
//

#import "DependencyStore.h"

@interface DependencyStore ()

@property (nonatomic, strong) NSMutableDictionary *store;

@end

@implementation DependencyStore

- (NSMutableDictionary *)store {
    if (!_store) {
        _store = [NSMutableDictionary new];
    }
    
    return _store;
}

- (FileDependency *)dependencyWithFile:(NSString *)file {
    FileDependency *dependency = [self dependencyForFile:file];
    dependency.loc += [self LOCInFile:file];
    return dependency;
}

- (FileDependency *)dependencyForFile:(NSString *)file {
    NSString *filename = [self extractedFileName:file];
    
    FileDependency *dependency = self.store[filename];
    
    if (!dependency) {
        dependency = [[FileDependency alloc] initWithName:filename];
        self.store[filename] = dependency;
    }
    
    return dependency;
}

- (NSString *)extractedFileName:(NSString *)fileName {
    return [[fileName lastPathComponent] stringByDeletingPathExtension];
}

- (NSUInteger)LOCInFile:(NSString *)file {
    NSString *fileContents = [NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:NULL];
    NSString *tmpString = [fileContents stringByReplacingOccurrencesOfString:@"/t" withString:@""];
    NSString *filteredString = [tmpString stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSMutableArray *lines = [[filteredString componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] mutableCopy];
    [lines removeObject:@""];
    return [lines count];
}

- (NSArray *)dependencies {
    return [self.store allValues];
}

@end
