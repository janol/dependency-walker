//
//  FileDependency.m
//  DependencyWalker
//
//  Created by Jan Olbrich on 04.07.15.
//  Copyright (c) 2015 Jan Olbrich. All rights reserved.
//

#import "FileDependency.h"

@interface FileDependency ()

@property (nonatomic, strong) NSMutableSet *dependencies;
@property (nonatomic, strong, readwrite) NSString *name;

@end

@implementation FileDependency

- (instancetype)initWithName:(NSString *)fileName {
    self = [super init];
    
    if (self) {
        self.name = fileName;
    }
    
    return self;
}

- (NSMutableSet *)dependencies {
    if (!_dependencies) {
        _dependencies = [NSMutableSet new];
    }
    return _dependencies;
}

- (NSArray *)fileDependencies {
    return [self.dependencies allObjects];
}

- (void)addFileDependency:(NSString *)file {
    if ([file isEqualToString:self.name]) {
        return;
    }
    
    [self.dependencies addObject:file];
}

@end
