//
//  GrapherDelegate.h
//  DependencyWalker
//
//  Created by Jan Olbrich on 04.07.15.
//  Copyright (c) 2015 Jan Olbrich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DependencyGrapherDelegate <NSObject>

- (void)graphDepencies:(NSArray *)dependencies toFile:(NSString *)filePath;

@end
