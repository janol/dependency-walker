//
//  GraphVizDependencyGrapher.m
//  DependencyWalker
//
//  Created by Jan Olbrich on 04.07.15.
//  Copyright (c) 2015 Jan Olbrich. All rights reserved.
//

#import "GraphVizDependencyGrapher.h"
#import "FileDependency.h"

@implementation GraphVizDependencyGrapher

- (void)graphDepencies:(NSArray *)dependencies toFile:(NSString *)filePath {
    NSMutableString *dependencyGraph = [NSMutableString new];
    
    [dependencyGraph appendString:@"digraph graphname {\n"];
    
    for (FileDependency *dependency in dependencies) {
        if (dependency.loc == 0) {
            continue;
        }
        
        for (NSString *dep in [dependency fileDependencies]) {
            [dependencyGraph appendString:[NSString stringWithFormat:@"\t\"%@\" -> \"%@\";\n", dependency.name, dep]];
        }
        
        if (dependency.loc > 3000) {
            [dependencyGraph appendString:[NSString stringWithFormat:@"\t\"%@\"[label=\"%@(%lu)\", fillcolor = red, style=filled];\n", dependency.name, dependency.name, dependency.loc]];
        }
        else if (dependency.loc > 1000) {
            [dependencyGraph appendString:[NSString stringWithFormat:@"\t\"%@\"[label=\"%@(%lu)\", fillcolor = yellow, style=filled];\n", dependency.name, dependency.name, dependency.loc]];
        }
        else {
            [dependencyGraph appendString:[NSString stringWithFormat:@"\t\"%@\"[label=\"%@(%lu)\"];\n", dependency.name, dependency.name, dependency.loc]];
        }
    }
    
    
    [dependencyGraph appendString:@"}"];
    
    [dependencyGraph writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

@end
