//
//  XCodeParser.m
//  DependencyWalker
//
//  Created by Jan Olbrich on 04.07.15.
//  Copyright (c) 2015 Jan Olbrich. All rights reserved.
//

#import "XCodeParser.h"

@interface XCodeParser ()

@property (nonatomic, strong) NSMutableArray *fileList;

@end

@implementation XCodeParser

- (NSArray *)fileListFromProjectPath:(NSString *)projectPath {
    NSString *projectFile = [projectPath stringByAppendingString:@"/project.pbxproj"];
    NSDictionary * myDictionary = [NSDictionary dictionaryWithContentsOfFile:projectFile];
    
    self.fileList = [NSMutableArray new];
    NSString *mainKey;
    NSString *rootpath;
    for (NSDictionary *dict in [myDictionary[@"objects"] allObjects]) {
        if (![dict[@"isa"] isEqualToString:@"PBXProject"] ) {
            continue;
        }
        
        mainKey = dict[@"mainGroup"];
        rootpath = [NSString stringWithFormat:@"%@%@", [projectPath stringByDeletingLastPathComponent], dict[@"projectDirPath"]];
        NSLog(@"%@", dict[@"mainGroup"]);
        
        /*if (!dict[@"lastKnownFileType"]) {
            continue;
        }
        
        if ([dict[@"lastKnownFileType"] rangeOfString:@"sourcecode"].location == NSNotFound) {
            continue;
        }
        
        [fileList addObject:[self convertLocalFilePath:dict[@"path"] withProjectFilePath:projectPath]];*/
    }
    [self extractFilePathFromProject:myDictionary[@"objects"] forObjectKey:mainKey andPath:rootpath];
    
    return [self.fileList copy];
}

- (void)extractFilePathFromProject:(NSDictionary *)project forObjectKey:(NSString *)key andPath:(NSString *)path {
    if ([project[key][@"isa"] isEqualToString:@"PBXFileReference"]) {
        if ([project[key][@"lastKnownFileType"] rangeOfString:@"sourcecode"].location != NSNotFound) {
            NSString *preciseFilePath = [NSString stringWithFormat:@"%@/%@",path, project[key][@"path"]];
            [self.fileList addObject:preciseFilePath];
        }
        
        return;
    }
    
    for (NSString *childKey in project[key][@"children"]) {
        NSString *precisePath;
        if (project[key][@"path"]) {
            precisePath = [NSString stringWithFormat:@"%@/%@",path, project[key][@"path"]];
        }
        else {
            precisePath = path;
        }
        
        [self extractFilePathFromProject:project forObjectKey:childKey andPath:precisePath];
    }
}

- (NSString *)convertLocalFilePath:(NSString *)localFilePath withProjectFilePath:(NSString *)projectFilePath {
    NSMutableString *shortedProjectPath = [[projectFilePath stringByDeletingPathExtension] mutableCopy];
    [shortedProjectPath appendString:@"/"];
    [shortedProjectPath appendString:localFilePath];
    
    return [shortedProjectPath copy];
}

@end
